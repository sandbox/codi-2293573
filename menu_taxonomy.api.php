<?php

/**
 * @file
 * Menu Taxonomy API
 * API documentation file for the Menu Taxononmy API module.
 */

/**
 * When a menu item is created for a taxonomy term, notify other modules.
 *
 * @param $link
 *   An object representing a single row from the {menu_links} table.
 *   This object defines the menu link and can be used to load additional
 *   data using menu_get_item().
 * @param $term
 *   The term object being acted upon.
 *
 * @see menu_get_item()
 * @see menu_save_item()
 */
function hook_menu_taxonomy_insert($link, $term) {
  // Store data in my custom table, which tracks the owners of nodes
  // placed in the site menu.
  $record = array(
    'tid' => $term->tid,
    'mlid' => $link->mlid,
    'uid' => $term->uid,
  );
  drupal_write_record('mytable', $record);
}

/**
 * When a taxonomy term or its menu item are updated, notify other modules.
 *
 * Note that this hook runs for each menu item that belongs to the term
 * (yes, core allows that), so normally you would use $link->mlid as the
 * primary key.
 *
 * @param $link
 *   An object representing a single row from the {menu_links} table.
 *   This object defines the menu link and can be used to load additional
 *   data using menu_get_item().
 * @param $term
 *   The term object being acted upon.
 */
function hook_menu_taxonomy_update($link, $term) {
  // Update data in my custom table, which tracks the owners of terms
  // placed in the site menu.
  $record = array(
    'tid' => $term->tid,
    'mlid' => $link->mlid,
    'uid' => $term->uid,
  );
  drupal_write_record('mytable', $record, array('mlid', 'uid'));
}

/**
 * When a taxonomy term or its menu item are deleted, notify other modules.
 *
 * Note that this hook runs for each menu item that belongs to the term
 * (yes, core allows that), so normally you would use $link->mlid as the
 * primary key.
 *
 * @param $link
 *   An object representing a single row from the {menu_links} table.
 *   This object defines the menu link and can be used to load additional
 *   data using menu_get_item().
 * @param $term
 *   The term object being acted upon.
 */
function hook_menu_taxonomy_delete($link, $term) {
  // Delete data in my custom table, which tracks the owners of terms
  // placed in the site menu.
  db_delete('mytable')
    ->condition('mlid', $link->mlid)
    ->execute();
}
