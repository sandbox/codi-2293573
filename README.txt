
Menu Taxonomy API
Manages relationships between taxonomy ids and menu links.

CONTENTS
--------

1. Introduction
2. Installation
3. Use Cases
4. Developer Notes
5. API Hooks
6. The {menu_taxonomy} Table

----
1. Introduction

The Menu Taxonomy API project fills in one of the "missing tables" in Drupal 7.
This module does nothing on its own. Instead, it creates and maintains
a {menu_taxonomy} table. This table maps the relationship between a taxonomy term
and its menu link (if any).

The module requires the optional core Menu module to be enabled and definitely
benefits from the taxonomy_menu_form.

By itself, this module has no functionality. When terms are assigned to a
site menu, records are created to capture that relationship. If the term or
menu link is deleted, the record is likewise deleted.

The module does fire a set of internal hooks when term-based menus are
created, edited, or deleted. See section 4 for details.

----
2. Installation

To install the module, simply place the folder in your modules directory
(typically 'sites/all/modules'.

Then go to the Module administration page and activate the module. It
can be found listed under the 'Other' category.

----
3. Use Cases

The two most obvious uses for this module are:

-- To build an access control module that leverages the menu hierarchy.
-- To integrate menu hierarchies into the Views module.

The purpose, in both cases, is to allow developers and site builders to use
the menu system as the sole metaphor for a site's structure. For many
small to mid-size Drupal sites, this approach offers advantages over being
forced to use a taxonomy-based (or other) solution).

----
4. Developer Notes

Aside from the core CRUD functions, this module is of limited use. There
are a few handy functions included, such as menu_taxonomy_tree(), but they
may not be suitable for all uses. Feel free to develop your own functions
and tricks.

----
5. API Hooks

At the core of the module are three simple CRUD hooks. They each pass
the same arguments:

  -- hook_menu_taxonomy_term_insert($link, $term)
  -- hook_menu_taxonomy_term_update($link, $term)
  -- hook_menu_taxonomy_delete($link, $term)

These functions pass the arguments:

  -- $link
  The menu link being acted upon. (Taken from the {menu_links} table as an
  object).
  -- $term
  The complete term object being acted upon.

NOTE: Using menu_get_item() here returns a router item, not the data
found in {menu_links}. So instead, we load the $item from the {menu_links}
table directly.

NOTE: Terms can be assigned to multiple menu links. In these cases, the
hook should fire once for each menu link.

There are additional functions that you may find useful. Check the inline
Doxygen comments for details.

----
6. The {menu_taxonomy} Table

The {menu_taxonomy} table is deliberately simple. It contains a primary key
and two columns:

  -- tid
  Integer, unsigned.
  Foreign key to the {taxonomy_term_data} table.

  -- mlid
  Primary key. Integer, unsigned.
  Foreigh key to the {menu_links} table.
